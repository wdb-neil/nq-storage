function sessionService() {
    var sess = this;

    sess.error = {};
    sess.error.objectExists = "Object already exists!";
    sess.error.keyNotFound = "Key does not exist!";

    /**
     * Append value to key-value-pair if the object does not exist
     *
     * @param object { tbl: string, value: mixed, callback: callable }
     */
    sess.insert = function (params) {
        var tblvalue = sess.all({ tbl: params.tbl });
        var exists = _.find(tblvalue, params.value);

        if (exists !== undefined) {
            params.d.reject(new Response(false, sess.error.objectExists));
            return;
        }

        if (params.key !== undefined && params.key !== null) {
            tblvalue[params.key] = params.value;
        } else {
            tblvalue.push(params.value);
        }

        var transaction = sessionStorage.setItem(params.tbl, JSON.stringify(tblvalue));

        params.d.resolve(new Response(true, transaction));
    }

    /**
     * Fetch value of table provided and extract the element that matches key.
     *
     * @param object { tbl: string, key: int, callback: callable }
     */
    sess.get = function (params) {
        var tblvalue = sess.all({ tbl: params.tbl });
        var key = params.key;
        var exists = typeof tblvalue[key];

        if (exists === undefined) {
            params.d.reject(new Response(false, sess.error.keyNotFound));
            return;
        }

        params.d.resolve(new Response(true, tblvalue[key]));
    }

    /**
     * Update record from session db
     *
     * @param object { tbl: string, key: int, value: mixed, d: object }
     */
    sess.update = function (params) {
        var tblvalue = sess.all({ tbl: params.tbl });
        var exists = typeof tblvalue[params.key];

        if (exists === undefined) {
            params.d.reject(new Response(false, sess.error.keyNotFound));
            return;
        }

        console.log("KEY: ", params.key);

        console.log("UPDATING: ", tblvalue[params.key]);

        tblvalue[params.key] = params.value;

        var transaction = sessionStorage.setItem(params.tbl, JSON.stringify(tblvalue));

        params.d.resolve(new Response(true, params.value));
    }

    /**
     * Remove object with given key from session
     *
     * @param object { tbl: string, key: int, service: object }
     */
    sess.remove = function (params) {
        var tblvalue = sess.all({ tbl: params.tbl });
        var key = params.key;
        var exists = typeof tblvalue[key];

        if (exists === undefined) {
            params.d.reject(new Response(false, sess.error.keyNotFound));
            return;
        }

        tblvalue.splice(key, 1);

        var transaction = sessionStorage.setItem(params.tbl, JSON.stringify(tblvalue));

        params.d.resolve(new Response(true, params));
    }

    /**
     * Fetch value of table provided and parse to array.
     *
     * @param object { tbl: string, service: object }
     */
    sess.all = function (params) {
        var transaction = sessionStorage.getItem(params.tbl);

        var result = transaction === null ? [] : JSON.parse(transaction);

        if (typeof params.d !== "undefined") {
            params.d.resolve(new Response(true, result));
        } else {
            return result;
        }
    }
}
