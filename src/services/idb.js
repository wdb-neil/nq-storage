function idbService(dbname, version, upgradeCallbacks) {
    var idb = this;

    idb.database = null;
    idb.dbname = dbname;
    idb.version = version;
    idb.upgradeCallbacks = upgradeCallbacks;
    idb.transactionQueue = [];

    /**
     * Inserts a new record into the database.
     *
     * @param object { tbl: string, value: mixed, d: object }
     */
    idb.insert = function (params) {
        if (idb.database === null) {
            queueTransaction({ method: idb.insert, params: params });
            return;
        }

        if (params.key !== null) {
            var request = openStore(params.tbl, "readwrite").add(params.value, params.key);
        } else {
            var request = openStore(params.tbl, "readwrite").add(params.value);

        }

        request.onerror = function (e) {
            params.d.reject(new Response(false, e));
        }

        request.onsuccess = function (e) {
            params.d.resolve(new Response(true, e));
        }
    }

    /**
     * Returns the record that matches the given key.
     *
     * @param object { tbl: string, key: int, d: object }
     */
    idb.get = function (params) {
        if (idb.database === null) {
            queueTransaction({ method: idb.get, params: params });
            return;
        }

        var request = openStore(params.tbl, "readonly").get(params.key);

        request.onerror = function (e) {
            params.d.reject(new Response(false, e));
        }

        request.onsuccess = function (e) {
            params.d.resolve(new Response(true, e.target.result));
        }
    }

    /**
     * Update record from idb
     *
     * @param object { tbl: string, key: int, value: mixed, d: object }
     */
    idb.update = function (params) {
        if (idb.database === null) {
            queueTransaction({ method: idb.update, params: params });
        }

        var request = openStore(params.tbl, "readwrite").put(params.value, params.key);

        request.onerror = function (e) {
            params.d.reject(new Response(false, e));
        }

        request.onsuccess = function (e) {
            params.d.resolve(new Response(true, params.value));
        }
    }

    /**
     * Remove record from idb
     *
     * @param object { tbl: string, key: int, d: object }
     */
    idb.remove = function (params) {
        if (idb.database === null) {
            queueTransaction({ method: idb.remove, params: params });
            return;
        }

        var request = openStore(params.tbl, "readwrite").delete(params.key);

        request.onerror = function (e) {
            params.d.reject(new Response(false, e));
        }

        request.onsuccess = function (e) {
            params.d.resolve(new Response(true, params));
        }
    }

    /**
     * Fetches all records from a store.
     *
     * @param object { tbl: string, d: object }
     */
    idb.all = function (params) {
        var result = [];

        if (idb.database === null) {
            queueTransaction({ method: idb.all, params: params });
            return;
        }

        var request = openStore(params.tbl, "readonly").openCursor();

        request.onerror = function (e) {
            params.d.reject(new Response(false, e));
        }

        request.onsuccess = function (e) {
            var record = e.target.result;

            if (record) {
                result.push(record.value);
                record.continue();
            } else {
                params.d.resolve(new Response(true, result));
            }
        }
    }

    /////////////////////////

    /**
     * Run queued functions stored when indexedDB is not ready.
     */
    idb.runQueued = function () {
        for (var i = 0; i < idb.transactionQueue.length; i++) {
            idb.transactionQueue[i].method(idb.transactionQueue[i].params);
        }
    }

    /**
     * Returns an objectStore.
     *
     * @param string tbl
     * @param string transactionType
     */
    function openStore(tbl, transactionType) {
        var transaction = idb.database.transaction([tbl], transactionType);
        return transaction.objectStore(tbl);
    }

    /**
     * Store transactions in a queue (array) to be executed later when indexedDB is ready.
     *
     * @param object { method: callable, params: object }
     */
    function queueTransaction(transaction) {
        idb.transactionQueue.push(transaction);
    }

    /**
     * Initialize the service
     */
    function init() {
        var connection = indexedDB.open(idb.dbname, idb.version);

        connection.onupgradeneeded = function (e) {
            console.log("Upgrading indexedDB");
            idb.upgradeCallbacks[idb.version - 1](e.target.result);
        }

        connection.onsuccess = function (e) {
            console.log("IndexedDB connected");
            idb.database = e.target.result;
            idb.runQueued();
        }

        connection.onerror = function (e) {
            console.error(e);
        }
    }

    init();
}
