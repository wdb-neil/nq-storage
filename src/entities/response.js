function Response(success, message) {
    return {
        success: success,
        message: message,
    }
}
