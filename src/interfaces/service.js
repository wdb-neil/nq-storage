function Storage(opt) {
    var $q = opt.$q;
    var idbSupported = "indexedDB" in window ? true : false;
    var webStorageSupported = typeof(Storage) !== undefined ? true : false;

    if (opt.service === 'idb' && idbSupported) {
        var service = new idbService(opt.dbname, opt.version, opt.callbacks);
    } else if (webStorageSupported) {
        var service = new sessionService();
    }

    /**
     * Insert a record using the service in use.
     *
     * @param string tbl
     * @param mixed value
     */
    this.insert = function (tbl, value, key) {
        var d = $q.defer();

        service.insert({
            tbl: tbl,
            value: value,
            key: key,
            d: d,
        });

        return d.promise;
    }

    /**
     * Return a specific record using the service in use.
     *
     * @param string tbl
     * @param int key
     */
    this.get = function (tbl, key) {
        var d = $q.defer();

        service.get({
            tbl: tbl,
            key: key,
            d: d,
        });

        return d.promise;
    }

    /**
     * Update the record with the given key
     *
     * @param string tbl
     * @param int key
     */
    this.update = function (tbl, value, key) {
        var d = $q.defer();

        service.update({
            tbl: tbl,
            value: value,
            key: key,
            d: d,
        });

        return d.promise;
    }

    /**
     * Delete the object with the given key from storage
     *
     * @param string tbl
     * @param int key
     */
    this.remove = function (tbl, key) {
        var d = $q.defer();

        service.remove({
            tbl: tbl,
            key: key,
            d: d,
        });

        return d.promise;
    }

    /**
     * Fetch all records using the service in use.
     *
     * @param string tbl
     */
    this.all = function (tbl) {
        var d = $q.defer();

        service.all({
            tbl: tbl,
            d: d,
        });

        return d.promise;
    }
}
