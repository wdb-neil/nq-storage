(function () {

    'use strict';

    var app = angular.module('nq.storage', []);

    app.provider('$storage', function () {
        var idbSupported = "indexedDB" in window ? true : false;

        this.service = 'idb';
        this.dbname = 'default';
        this.version = 0;
        this.upgradeCallbacks = [];

        this.primaryService = function (service) {
            this.service = service;
            return this;
        }

        this.connect = function (name) {
            this.dbname = name;
            return this;
        }

        this.upgrade = function (callback) {
            this.version++;
            this.upgradeCallbacks.push(callback);
            return this;
        }

        this.$get = ['$q', function ($q) {
            return new Storage({
                $q: $q,
                service: this.service,
                dbname: this.dbname,
                version: this.version,
                callbacks: this.upgradeCallbacks,
            });
        }];
    });

})();
