var gulp = require('gulp'),
    sass = require('gulp-sass'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    inject = require('gulp-inject'),
    uglify = require('gulp-uglify'),
    cleanCss = require('gulp-clean-css'),
    npmFiles = require('gulp-main-npm-files'),
    bowerFiles = require('main-bower-files'),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('bower', function () {
    gulp.src(bowerFiles())
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min',
        }))
        .pipe(gulp.dest('src/vendors'));
});

gulp.task('npm', function () {
    gulp.src(npmFiles())
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min',
        }))
        .pipe(gulp.dest('src/vendors'));
});

gulp.task('js', function() {
    gulp.src([
            'src/vendors/*.js',
            'src/**/*.js',
            'src/*.js',
            'src/app.js',
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('nqstorage.min.js'))
        .pipe(uglify().on('error', gutil.log))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('public'));
});

gulp.task('watch', function() {
    gulp.watch('src/**/*.js', ['js']);
});

gulp.task('default', ['bower', 'npm', 'js', 'watch']);
